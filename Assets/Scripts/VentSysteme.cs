﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentSysteme : MonoBehaviour
{
    public int valueVent;
    public GameObject player;
    public bool ventUse;
    public Vent[] allVents;
    
    void Update()
    {
      
    }
    public void buttonVent(int value)
    {
        valueVent += value;
    }

    public void useVent(string arrowname)
    {
        foreach (var vent in allVents)
        {
            if (vent.arrow.name == arrowname)
            {
                player.transform.position = vent.destination.transform.position;
            }
        }
    }
}
[Serializable]
public class Vent
{
    public GameObject arrow, destination;
    
}