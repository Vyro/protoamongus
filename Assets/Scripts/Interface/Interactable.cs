﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Interactable
{
    void Use(PlayerControler player);
}
