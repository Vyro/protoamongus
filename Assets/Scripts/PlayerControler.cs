﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Cette classe implémente les mouvements
 *  et l'interaction (touche espace) avec les éléments de la map (Taches, bouttons, ...)
 */
public class PlayerControler : MonoBehaviour
{
    [SerializeField] private string name;
    [SerializeField] private float moveSpeed = 5.0f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private List<Task> tasksCrewmate = new List<Task>();
    [SerializeField] private bool isImpostor;
    [SerializeField] private bool isDead;
    [SerializeField] private bool pnj;
    private bool isUsingTask;
    private bool canReport;
    private Vector2 _movement;
    private List<Interactable> _interactions;
    public GameObject reportButton;


    // Start is called before the first frame update
    void Start()
    {
        // L'imposteur n'a pas de tâches
        if (isImpostor && tasksCrewmate.Count > 0)
        {
            Debug.LogWarning("L'imposteur n'a pas besoin de tâches !");
            tasksCrewmate.RemoveRange(0, tasksCrewmate.Count);
        }
        
        _interactions = new List<Interactable>();

        foreach (Task task in tasksCrewmate)
        {
            if (task != null)
            {
                task.iconTask.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(pnj)
            return;
        // Movements of the player
        if (!isUsingTask && !isDead)
        {
            Movements();
        }
        
        
        // S'il appuie sur espace
        if (Input.GetKey(KeyCode.Space) && _interactions.Count > 0 && !isUsingTask)
        {
            // TODO : Essayer de détecter le premier élément Interactable aux alentours du personnage
            Debug.Log("Use");
            // Utilise le premier élément interactif rencontré
            _interactions[0].Use(this);
            isUsingTask = true;
        }
    }

    public void Killed()
    {
        Debug.Log(name + " is dead");
        if (!isDead)
        {
            isDead = true;
        }
        Destroy(this);
    }

    public void EndTask(Task task)
    {
        isUsingTask = false;
        
        if (task != null)
        {
            tasksCrewmate.Remove(task);
        }
    }

    public void UseButton()
    {
        if (_interactions.Count > 0 && !isUsingTask)
        {
            Debug.Log("Use");
            // Utilise le premier élément interactif rencontré
            _interactions[0].Use(this);
            isUsingTask = true;
        }
    }

    public void ReportButton()
    {
        Debug.Log("A body was find !");
        reportButton.GetComponent<Button>().interactable = false;
    }

    void Movements()
    {
        _movement.x = Input.GetAxisRaw("Horizontal");
        _movement.y = Input.GetAxisRaw("Vertical");
        // Adapt the diagonal movement
        _movement.Normalize();
    }
    
    void FixedUpdate()
    {
        rb.MovePosition(rb.position + _movement * (moveSpeed * Time.fixedDeltaTime));
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        Interactable interactable = other.gameObject.GetComponent<Interactable>();

        if (interactable != null && !_interactions.Contains(interactable))
        {
            // Si l'élément interactif est une tâche
            Task task = other.GetComponent<Task>();
            if (task != null && tasksCrewmate.Contains(task) && !isImpostor || task == null)
            {
                _interactions.Add(interactable);
            }
        }
        
        // Si l'élément interactif est un crewmate mort
        PlayerControler player = other.GetComponent<PlayerControler>();
        if (player != null && player.isDead)
        {
            reportButton.GetComponent<Button>().interactable = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Interactable interactable = other.gameObject.GetComponent<Interactable>();
        
        // Vire l'objet Interactable quand celui ci sort de la zone du personnage
        if (interactable != null)
        {
            _interactions.Remove(interactable);
        }
    }
}