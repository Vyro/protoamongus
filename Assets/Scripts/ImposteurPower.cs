﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImposteurPower : MonoBehaviour
{
    public float timer, maxtimer;
    public GameObject focus;
    void Update()
    {
        timer += Time.deltaTime;
    }
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !focus)
        {
            focus = other.gameObject;
        }
        if (other.CompareTag("vent") && !focus)
        {
            focus = other.gameObject;
        }
    }
    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("vent"))
        {
            focus = null;
        }

        if (other.CompareTag("Player"))
        {
            focus = null;
        }
    }
    public void buttonVent()
    {
        if (focus != null)
        {
            gameObject.transform.position = focus.transform.position;
            gameObject.transform.rotation = focus.transform.rotation;
        }
    }
    public void buttonKill()
    {
        if (timer >= maxtimer)
        {
            timer = 0;
            gameObject.transform.position = focus.transform.position;
            gameObject.transform.rotation = focus.transform.rotation;
            focus.GetComponent<PlayerControler>().Killed();
        }
    }
}
