﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class FieldOfViewTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Instance new Mesh
        Mesh mesh = new Mesh();
        // Fill in the component MeshFilter
        GetComponent<MeshFilter>().mesh = mesh;

        // Field of View Values
        float fov = 90f;
        Vector3 origin = Vector3.zero;
        int rayCount = 50 ;
        float angle = 0f;
        float angleIncrease = fov / rayCount;
        float viewDistance = 50f;
        
        // Raycount + 1 (for the origin) and + 1 (for the ray 0) 
        Vector3[] verticies = new Vector3[rayCount + 1 + 1];
        Vector2[] uv = new Vector2[verticies.Length];
        int[] triangles = new int[rayCount * 3];

        verticies[0] = origin;
        
        int vertexIndex = 1;
        int triangleIndex = 0;
        for (int i = 0; i <= rayCount; i++)
        {
            Vector3 vertex;
            RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, UtilsClass.GetVectorFromAngle((int) angle), viewDistance);
            
            if (raycastHit2D.collider == null)
            {
                // No hit
                vertex = origin + UtilsClass.GetVectorFromAngle((int) angle) * viewDistance;
            }
            else
            {
                // Hit object
                vertex = raycastHit2D.point;
            }
            
            verticies[vertexIndex] = vertex;
            
            if (i > 0)
            {
                triangles[triangleIndex + 0] = 0;
                triangles[triangleIndex + 1] = vertexIndex - 1;
                triangles[triangleIndex + 2] = vertexIndex;

                triangleIndex += 3;
            }

            vertexIndex++;
            angle -= angleIncrease;
        }

        // Build the mesh
        mesh.vertices = verticies;
        mesh.uv = uv;
        mesh.triangles = triangles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
