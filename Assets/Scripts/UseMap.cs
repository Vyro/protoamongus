﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseMap : MonoBehaviour
{
    private bool isMapOpen;
    [SerializeField] private GameObject carteUI;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (!isMapOpen)
            {
                OpenMap();
            }
            else
            {
                CloseMap();
            }
        }
    }

    void OpenMap()
    {
        isMapOpen = true;
        carteUI.SetActive(true);
    }

    void CloseMap()
    {
        isMapOpen = false;
        carteUI.SetActive(false);
    }
}
