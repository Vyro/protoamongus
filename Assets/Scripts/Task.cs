﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task : MonoBehaviour, Interactable
{
    public string name;
    public TypeTask task;
    public TaskGame game;
    private PlayerControler _player;
    public GameObject iconTask;

    public enum TypeTask
    {
        Common,
        Download,
        Short,
        Long
    }

    public void Use(PlayerControler player)
    {
        _player = player;
        Debug.Log(player.name + "Use" + name);
        game.gameObject.SetActive(true);
        game.Play(this);
    }

    public void End(bool finished)
    {
        game.gameObject.SetActive(false);
        if (finished)
        {
            _player.EndTask(this);
            iconTask.SetActive(false);
        }
        else
        {
            _player.EndTask(null);
        }
    }
}
