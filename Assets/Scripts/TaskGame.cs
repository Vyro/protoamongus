﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TaskGame : MonoBehaviour
{
    private Task _task;

    public virtual void Play(Task task)
    {
        _task = task;
    }

    protected void Stop(bool finished)
    {
        _task.End(finished);
    }
}
