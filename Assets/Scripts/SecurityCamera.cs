﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityCamera : MonoBehaviour, Interactable
{
    public GameObject panel;
    private PlayerControler _player;

    public void Use(PlayerControler player)
    {
        _player = player;
        panel.SetActive(true);
    }

    public void End()
    {
        _player.EndTask(null);
        panel.SetActive(false);
    }
}
