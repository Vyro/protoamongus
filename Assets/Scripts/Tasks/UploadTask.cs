﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UploadTask : TaskGame
{
    public Text text;
    public float timer;
    private bool _uploading;
    private float _endTime;
    private float _startingTime;
    
    public override void Play(Task task)
    {
        base.Play(task);
        text.text = "0%";
        _uploading = false;
    }

    public void Clic()
    {
        if (!_uploading)
        {
            _uploading = true;
            _endTime = Time.time + timer;
            _startingTime = Time.time;
        }
    }

    private void Update()
    {
        if (_uploading)
        {
            text.text = ((int)((Time.time - _startingTime)/(_endTime - _startingTime) * 100)) + "%";
        }

        if (_uploading && Time.time >= _endTime)
        {
            Stop(true);
        }
    }
}
